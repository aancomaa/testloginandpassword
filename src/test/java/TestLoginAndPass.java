import dto.Auth;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class TestLoginAndPass {

    public WebDriver driver;
    private String successUsername = "tomsmith";
    private String failedUsername = "LELE";
    private String failedPassword = "SecretPassword!";
    private String successPassword = "SuperSecretPassword!";

    @BeforeMethod
    public void setupClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://the-internet.herokuapp.com/login");
    }

    @Test(description = "Корректный логин и пароль", priority = 0)
    public void testLoginAndPass() {

        LoginPage loginPage = new LoginPage(driver);
        Auth successUser = new Auth(successUsername, successPassword);
        HomePage homePage = loginPage.successLoginWithUsernameAndPassword(successUser);
        loginPage.checkThatLoginIsSuccess();
    }

    @Test(description = "Тест некорректного логина", priority = 0)
    public void testIncorrectLogin() {

        LoginPage loginPage = new LoginPage(driver);
        Auth failedUser = new Auth(failedUsername, successPassword);
        loginPage = loginPage.failedLoginWithUsernameAndPassword(failedUser);
        loginPage.checkThatUsernameIsFailed();
    }

    @Test(description = "Тест некорректного пароля", priority = 0)
    public void testIncorrectPass() {

        LoginPage loginPage = new LoginPage(driver);
        Auth failedUser = new Auth(successUsername, failedPassword);
        loginPage = loginPage.failedLoginWithUsernameAndPassword(failedUser);
        loginPage.checkThatPasswordIsFailed();
    }

    @AfterMethod
    public void closeBrowser() {
        driver.close();
    }

}