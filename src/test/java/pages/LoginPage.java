package pages;

import dto.Auth;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import static org.testng.Assert.assertTrue;

public class LoginPage {

    private WebDriver driver;

    @FindBy(name = "username")
    private WebElement usernameField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(id = "flash")
    private WebElement flashField;

    @FindBy(xpath = "//*[contains(text(),'Your password is invalid!')]")
    private WebElement failedPasswordText;

    @FindBy(xpath = "//*[contains(text(),'Your username is invalid!')]")
    private WebElement failedUsernameText;

    @FindBy(xpath = "//*[contains(text(),'You logged into a secure area!')]")
    private WebElement successLoginText;

    public LoginPage(WebDriver driver) {
        if (!driver.getCurrentUrl().contains("/login")) {
            throw new IllegalStateException("This is not the page you are expected");
        }
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public HomePage successLoginWithUsernameAndPassword(Auth auth) {
        usernameField.sendKeys(auth.getUsername());
        passwordField.sendKeys(auth.getPassword());
        passwordField.sendKeys(Keys.ENTER);
        return new HomePage(driver);
    }

    public LoginPage failedLoginWithUsernameAndPassword(Auth auth) {
        usernameField.sendKeys(auth.getUsername());
        passwordField.sendKeys(auth.getPassword());
        passwordField.sendKeys(Keys.ENTER);
        return new LoginPage(driver);
    }

    public void checkThatPasswordIsFailed() {
        assertTrue(flashField.isDisplayed());
        assertTrue(failedPasswordText.isDisplayed());
    }

    public void checkThatUsernameIsFailed() {
        assertTrue(flashField.isDisplayed());
        assertTrue(failedUsernameText.isDisplayed());
    }

    public void checkThatLoginIsSuccess() {
        Assert.assertTrue(flashField.isDisplayed());
        Assert.assertTrue(successLoginText.isDisplayed());
    }
}